package io.elasticmedia.sdk.player.androidsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class ActivityMain extends AppCompatActivity {

    private final static String TAG = ActivityMain.class.getSimpleName();
    private final boolean USE_PLAYER_ACTIVITY_WITH_EXTERNAL_UI_PROCCESOR = true;
    private final boolean USE_PLAYER_ACTIVITY_WITH_UI_INSIDE_SDK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playVideo(USE_PLAYER_ACTIVITY_WITH_EXTERNAL_UI_PROCCESOR);
            }
        });
    }

    private void playVideo(boolean usePlayerActivityWithOutSideProccesor){

        String extUserId = "EmSampleAppAndroidExtUserId";

        String episodeId = "e450a30c-e3de-4644-b0ae-70338b389cb3"; //prod server mm 27/3/17
        String videoUrl = "https://cdn.elasticmedia.io/HLS/e450a30c-e3de-4644-b0ae-70338b389cb3/537280_2f66e7bda7bfb272a03dac4d2f3929b7/537280.m3u8";

        Intent intent;

        if (usePlayerActivityWithOutSideProccesor) {
            intent = new Intent(this, PlayerActivityWithUiProcessor.class);
        } else {
            intent = new Intent(this, PlayerActivity.class);
        }
        intent.putExtra(BasePlayerActivity.BUNDLE_EPISODE_ID, episodeId);     // THE VIDEO ID ON SERVER, FOR GETTING METADATA FOR ELASTIC PLAYBACK
        intent.putExtra(BasePlayerActivity.BUNDLE_CONTENT_URL, videoUrl);     // URL OF THE MP4 OR HSL
        intent.putExtra(BasePlayerActivity.BUNDLE_EXTERNAL_USER_ID, extUserId);      // SOME EXTERNAL USER ID

        intent.putExtra(BasePlayerActivity.BUNDLE_IS_DEBUG_VERBOSE, false);   // SETS DEBUG INFO ON OR OFF
        intent.putExtra(BasePlayerActivity.BUNDLE_IS_SERVER_DEV, false);      // SETS SERVER DEV OR PROD, DEV IS HOSTING EPISODES FOR TESTING
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_play) {
            playVideo(USE_PLAYER_ACTIVITY_WITH_UI_INSIDE_SDK);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
