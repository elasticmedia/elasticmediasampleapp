package io.elasticmedia.sdk.player.androidsample;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.elasticmedia.sdk.player.android.library.ElasticPlayerView;
import io.elasticmedia.sdk.player.android.library.ElasticSdkExtListener;
import io.elasticmedia.sdk.player.android.library.stateMachine.SavedState;
import io.elasticmedia.sdk.player.android.library.stateMachine.StateEvent;
import io.elasticmedia.sdk.player.android.library.transitionEffects.TransitionEffectProperties;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.DeckOfThreePagerAdapter;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.DeckOfThreeViewPager;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.DeckOfThreeViewPagerInterface;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.SwipeDirection;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.ViewPagePosition;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.ViewPagerOrientation;
import io.elasticmedia.sdk.player.android.library.utils.Utils;


public class PlayerActivityWithUiProcessor extends BasePlayerActivity implements ElasticSdkExtListener {

    private final static String TAG = PlayerActivityWithUiProcessor.class.getSimpleName();


    public static final String BUNDLE_JSON_SAVED_STATE = "BUNDLE_JSON_SAVED_STATE";

    private final Handler mUiHandler = new Handler(Looper.getMainLooper());
    private FrameLayout mUiActivityParent;
    private FrameLayout mUiCarouselLayer;
    private ElasticPlayerView mUiElasticPlayerView;
    private String mSavedStateStringified;
    private ExternalUiSkin1Processor mOutsideSdkSkin1UiEventProcessor = null;

    private boolean mIsUsingViewPager = true;
    private DeckOfThreeViewPager mUiDeckOfThreeViewPager;
    private DeckOfThreeViewPagerInterface mDeckOfThreeViewPagerInterface;
    private DeckOfThreePagerAdapter mDeckOfThreePagerAdapter;
    private volatile int mCurrentItemNumber = 1;
    private final Runnable mRunnableMoveToNextPage = new Runnable() {
        @Override
        public void run() {
            if (mDeckOfThreeViewPagerInterface != null){
                mDeckOfThreeViewPagerInterface.setViewPagePosition(ViewPagePosition.NEXT, true);
            }
        }
    };
    private final Runnable mRunnableReloadAllPagesIfPossible = new Runnable() {
        @Override
        public void run() {
            if (mDeckOfThreeViewPagerInterface != null){
                mDeckOfThreeViewPagerInterface.setCurrentPreviewPictureUrlIndex(mCurrentItemNumber - 1);
                mDeckOfThreeViewPagerInterface.reloadAllPagesIfPossible();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_player_with_ui_and_carousel);
        mUiActivityParent = (FrameLayout)findViewById(R.id.player_activity_parent);
        mUiCarouselLayer = (FrameLayout) mUiActivityParent.findViewById(R.id.carousel_layer);

        Bundle arguments = getIntent().getExtras();
        String mEpisodeID = arguments.getString(BUNDLE_EPISODE_ID);
        String url = arguments.getString(BUNDLE_CONTENT_URL);
        String externalUserID = arguments.getString(BUNDLE_EXTERNAL_USER_ID);
        boolean isDebugVerbose = arguments.getBoolean(BUNDLE_IS_DEBUG_VERBOSE);
        boolean isServerDev = arguments.getBoolean(BUNDLE_IS_SERVER_DEV);
        boolean isTFxForcedOff = arguments.getBoolean(BUNDLE_IS_TFX_FORCED_OFF);

        mSavedStateStringified = null;

        if (savedInstanceState != null) {
            String savedStateString = savedInstanceState.getString(BUNDLE_JSON_SAVED_STATE);
            if (TextUtils.isEmpty(savedStateString) == false) {
                resumeFromState(mSavedStateStringified);
                mSavedStateStringified = null;
                return;
            }
        }

        mUiElasticPlayerView = new ElasticPlayerView(this);
        mUiElasticPlayerView
                .setIsDebugVerbose(isDebugVerbose)
                .setIsServerDev(isServerDev)
                .setPublisherId(BuildConfig.PUBLISHER_ID_CH2)
                .setExternalUserId(externalUserID);

        if (isTFxForcedOff == false && mIsUsingViewPager == false) {

            mUiElasticPlayerView
                    .setTransitionEffectNext(TransitionEffectProperties.LAST_FRAME_SLIDE)
                    .setTransitionEffectPrevious(TransitionEffectProperties.LAST_FRAME_SLIDE)
                    .setTransitionEffectNoAction(null)
                    .setTransitionEffectFileNextNoAction(null);
        }

        addElasticPlayerView(mUiElasticPlayerView);

        try {
            Class.forName("io.elasticmedia.sdk.player.androidsample.ExampleInstrumentedTest");
            io.elasticmedia.sdk.player.android.library.utils.Utils.setTestModeState(true);
        } catch (final Exception e) {
        }

        mUiElasticPlayerView.play(mEpisodeID, url, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mUiElasticPlayerView != null) {
            mSavedStateStringified = mUiElasticPlayerView.getState();
            if (mUiElasticPlayerView.isReleased() == false) {
                mUiElasticPlayerView.releaseAll();
            }
            removeElasticPlayerView(mUiElasticPlayerView);
            removeDeckOfThreeViewPager(mUiDeckOfThreeViewPager);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }


    @Override
    protected void onResume() {
        super.onResume();
        resumeFromState(mSavedStateStringified);
    }

    private void resumeFromState(String savedState){
        if (savedState == null){
            return;
        }
        mUiElasticPlayerView = new ElasticPlayerView(this);
        addElasticPlayerView(mUiElasticPlayerView);
        mUiElasticPlayerView.resumeFromState(savedState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mUiElasticPlayerView != null) {
            mSavedStateStringified = mUiElasticPlayerView.getState();
            outState.putString(BUNDLE_JSON_SAVED_STATE, mSavedStateStringified);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private String convertSavedStateToString(SavedState savedState){
        Gson gson = new Gson();

        //Convert to string
        String jsonString = gson.toJson(savedState);

        return jsonString;
    }

    private SavedState convertStringToSavedStated(String savedStateString){
        Gson gson = new Gson();

        //Convert it back to object
        SavedState savedState = gson.fromJson(savedStateString, SavedState.class);

        return savedState;
    }

    private void removeElasticPlayerView(ElasticPlayerView elasticPlayerView){
        if (elasticPlayerView != null && elasticPlayerView.getParent() instanceof ViewGroup) {
            ((ViewGroup)(elasticPlayerView.getParent())).removeView(elasticPlayerView);
        }else{
            Log.w(TAG, "removeElasticPlayerView() no parent");
        }
    }

    private void removeDeckOfThreeViewPager(DeckOfThreeViewPager viewPager){
        if (viewPager != null && viewPager.getParent() instanceof ViewGroup) {
            ((ViewGroup)(viewPager.getParent())).removeView(viewPager);
        }else{
            Log.w(TAG, "removeDeckOfThreeViewPager() no parent");
        }
    }

    /**
     * Implicitly creates the ViewPager if needed
     * @param elasticPlayerView
     */
    private void addElasticPlayerView(final ElasticPlayerView elasticPlayerView){
        if (elasticPlayerView.getParent() != null){
            Log.d(TAG, "removeElasticPlayerView() already has a parent");
            return;
        }

        if (mOutsideSdkSkin1UiEventProcessor == null){
            mOutsideSdkSkin1UiEventProcessor = new ExternalUiSkin1Processor(
                    (FrameLayout) mUiActivityParent.findViewById(R.id.ui_layer),
                    (FrameLayout) mUiActivityParent.findViewById(R.id.debug_layer)
            );
        }

        mOutsideSdkSkin1UiEventProcessor.setElasticPlayerView(elasticPlayerView);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        if (mIsUsingViewPager == false) {
            mUiCarouselLayer.addView(elasticPlayerView, layoutParams);
        }else{
            mUiDeckOfThreeViewPager = new DeckOfThreeViewPager(this, ViewPagerOrientation.HORIZONTAL);
            mDeckOfThreeViewPagerInterface = new DeckOfThreeViewPagerInterface() {
                @Override
                public void reloadAllPagesIfPossible() {
                    mUiDeckOfThreeViewPager.reloadAllPagesIfPossible();
                }

                /**
                 *
                 * @param currentIndex this should 0 based
                 */
                @Override
                public void setCurrentPreviewPictureUrlIndex(int currentIndex) {
                    mUiDeckOfThreeViewPager.setCurrentPreviewPictureUrlIndex(currentIndex);
                }

                /**
                 * Can only SET once
                 *
                 * @param previewPictureUrls
                 * @param currentPreviewPictureUrlIndex this should 0 based
                 */
                @Override
                public void setPreviewPictureUrls(ArrayList<String> previewPictureUrls, int currentPreviewPictureUrlIndex) {
                    mUiDeckOfThreeViewPager.setPreviewPictureUrls(previewPictureUrls, currentPreviewPictureUrlIndex);
                }

                @Override
                public void setViewPagePosition(ViewPagePosition viewPagePosition, boolean animatePositionChange) {
                    mUiDeckOfThreeViewPager.setCurrentItem(viewPagePosition.getPosition(), animatePositionChange);
                }

                @Override
                public void setAllowedSwipeDirection(SwipeDirection allowedSwipeDirection) {
                    mUiDeckOfThreeViewPager.setAllowedSwipeDirection(allowedSwipeDirection);
                }

                @Override
                public View getPrimaryView() {
                    return elasticPlayerView;
                }

                @Override
                public void dispatchEvent(StateEvent stateEvent, Object object) {
                    elasticPlayerView.dispatchEvent(stateEvent, object);
                }

            };
            mDeckOfThreePagerAdapter = new DeckOfThreePagerAdapter(mDeckOfThreeViewPagerInterface);
            mUiDeckOfThreeViewPager.setAdapter(mDeckOfThreePagerAdapter);
            mUiDeckOfThreeViewPager.setPageMargin(Utils.convertDpToPixel(this, 9));

            mUiCarouselLayer.addView(mUiDeckOfThreeViewPager, layoutParams);
        }
        mUiElasticPlayerView.setEventListener(this);

        mOutsideSdkSkin1UiEventProcessor.setDeckOfThreeViewPagerInterface(mDeckOfThreeViewPagerInterface);
        mUiElasticPlayerView.setupOutSideSdkUi(mOutsideSdkSkin1UiEventProcessor);

    }

    @Override
    public void onEpisodeEnded(final String errorString) {
        Log.d(TAG, "onEpisodeEnded(): " + errorString);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "onEpisodeEnded() runnable: " + errorString);
                if (TextUtils.isEmpty(errorString) == false && Utils.isInAnyKindTestMode(PlayerActivityWithUiProcessor.this.getApplicationContext()) == false){
                    Toast.makeText(PlayerActivityWithUiProcessor.this, errorString, Toast.LENGTH_LONG).show();
                }
                if (mUiElasticPlayerView != null && mUiElasticPlayerView.isReleased() == false) {
                    mUiElasticPlayerView.releaseAll();
                }
                if (PlayerActivityWithUiProcessor.this.isFinishing() == false) {
                    PlayerActivityWithUiProcessor.this.finish();
                }
            }
        });
    }

    @Override
    public void onNextItem(final int itemNumber) {
        if (mIsUsingViewPager) {
            mCurrentItemNumber = itemNumber;
            mUiHandler.post(mRunnableReloadAllPagesIfPossible);
        }
    }

    @Override
    public void onPreviousItem(final int itemNumber) {
        if (mIsUsingViewPager){
            mCurrentItemNumber = itemNumber;
            mUiHandler.post(mRunnableReloadAllPagesIfPossible);
        }
    }

    /**
     * @return TRUE if should AutoPlay to Next Item
     */
    @Override
    public boolean onItemEnded() {
        boolean retValue;
        if (mIsUsingViewPager == false){
            retValue = true;
        }else {
            mUiHandler.post(mRunnableMoveToNextPage);
            retValue = false;
        }
        Log.d(TAG, "onItemEnded() " + retValue);
        return retValue;
    }
}
