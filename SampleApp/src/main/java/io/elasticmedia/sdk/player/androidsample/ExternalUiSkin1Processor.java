package io.elasticmedia.sdk.player.androidsample;

import android.animation.LayoutTransition;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.text.format.DateUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import io.elasticmedia.sdk.player.android.library.ElasticPlayerView;
import io.elasticmedia.sdk.player.android.library.stateMachine.StateContainer;
import io.elasticmedia.sdk.player.android.library.stateMachine.StateEvent;
import io.elasticmedia.sdk.player.android.library.ui.UiEvent;
import io.elasticmedia.sdk.player.android.library.ui.UiProcessor;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.DeckOfThreeViewPagerInterface;
import io.elasticmedia.sdk.player.android.library.ui.deckOfThree.ViewPagePosition;
import io.elasticmedia.sdk.player.android.library.utils.Utils;
import rm.com.youtubeplayicon.PlayIconDrawable;
import rm.com.youtubeplayicon.PlayIconView;

import static android.view.View.inflate;

public class ExternalUiSkin1Processor implements UiProcessor {
    private static final String TAG = ExternalUiSkin1Processor.class.getSimpleName();

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final int DEFAULT_USER_REQUESTED_SEEK_POSITION = Integer.MIN_VALUE;
    private static final float PLAY_PAUSE_MAX_ALPHA = 0.8F;
    private static final long AUTO_FADE_DELAY_MS = 3 * DateUtils.SECOND_IN_MILLIS;
    private static final long SHORT_FADE_MS = 125;
    private static final long LONG_FADE_MS = 1 * DateUtils.SECOND_IN_MILLIS;
    private static final long USER_REQUESTED_SEEK_POSTITON_WIGGLE_MS = DateUtils.SECOND_IN_MILLIS;
    private static final long RESET_USER_REQUESTED_SEEK_POSTITON_MAX_MS = 300;
    private static final int HIDDEN_VIEW = View.GONE;

    private static final boolean FORCE_SHOW = true;
    private static final boolean IS_SHOW_UI_EVERY_NEXT_PREVIOUS = false;
    private static final boolean DO_NOT_FORCE_SHOW = false;
    private static final boolean INCLUDE_PAUSE_PLAY = true;
    private static final boolean DO_NOT_INCLUDE_PAUSE_PLAY = false;



    private final Runnable RUNNABLE_RESET_USER_REQUESTED_SEEK_POSITION_MAX = new Runnable() {
        @Override
        public void run() {
            mIsUserSeekingPending = false;
            mUserRequestedSeekPosition = DEFAULT_USER_REQUESTED_SEEK_POSITION;
        }
    };
    FrameLayout mUiLayer, mDebugUiLayer;
    ElasticPlayerView mElasticPlayerView;

    private boolean mDidInflateUi = false;

    private RelativeLayout mUiSeekbarLayout;
    private SeekBar mUiSeekBar;
    private TextView mUiSeekbarEndTimeLabel;
    private LinearLayout mUiSegmentIndicatorsLayout;
    private FrameLayout mUiExtendButtonLayout;
    private ImageView mUiExtendBackground, mUiExtendButton;
    private PlayIconView mUiPausePlay;
    private ImageView mUiPrevoiusOptionVisualIndicator;
    private ImageView mUiNextOptionVisualIndicator;

    private ArrayList<View> mSegmentIndicators;
    private View[] mSegmentIndicatorsSimpleArray;
    private boolean mSwipeListening = false;
    private boolean mIsUserSeekingPending = false;
    private boolean mIsUserHoldingSeek = false;
    private long mUserRequestedSeekPosition = DEFAULT_USER_REQUESTED_SEEK_POSITION;
    private GestureDetectorCompat mGestureDetector;
    private long mPlaybackCurrentTimeMs = -1;
    private long mPlaybackEndTimeMs = -1;
    private long mTimeLastTapMs = -1;
    private boolean mIsViewVisible = false;
    private volatile StateContainer mCurrentStateContainer = null;

    private DeckOfThreeViewPagerInterface mDeckOfThreeViewPagerInterface;

    private float mPixelsInOneDPfloat;

    private StringBuilder sb = new StringBuilder();

    TextView mDebugUiPlayerId, mDebugUiPartName, mDebugUiStartTime, mDebugUiEndTime;

    public ExternalUiSkin1Processor(FrameLayout uiLayer, FrameLayout debugUiLayer ) {
        this.mUiLayer = uiLayer;
        this.mDebugUiLayer = debugUiLayer;
        this.mPixelsInOneDPfloat = Utils.getOneDpInPixelsFloat(uiLayer.getContext());
    }

    public void setElasticPlayerView(ElasticPlayerView elasticPlayerView){
        this.mElasticPlayerView = elasticPlayerView;
        updateDebugUiLayerVisibility();
    }

    public void setDeckOfThreeViewPagerInterface(DeckOfThreeViewPagerInterface deckOfThreeViewPagerInterface){
        this.mDeckOfThreeViewPagerInterface = deckOfThreeViewPagerInterface;
    }



    private void updateDebugUiLayerVisibility() {
        if (mElasticPlayerView.getIsDebugVerbose()){
            mDebugUiLayer.setVisibility(View.VISIBLE);
        }else{
            mDebugUiLayer.setVisibility(View.GONE);
        }
    }

    private void initialViewMode (StateContainer stateContainer) {
        updateDebugUiLayerVisibility();

        if (mDidInflateUi == false) {
            inflate(mElasticPlayerView.getContext(), R.layout.skin1_uilayer_outside_sdk, mUiLayer);
            mDidInflateUi = true;
        }

        mUiPrevoiusOptionVisualIndicator = (ImageView) mUiLayer.findViewById(R.id.swipe_left);
        mUiNextOptionVisualIndicator = (ImageView) mUiLayer.findViewById(R.id.swipe_right);

        mUiSegmentIndicatorsLayout = (LinearLayout) mUiLayer.findViewById(R.id.segment_indicators_layout);
        mUiExtendButtonLayout = (FrameLayout) mUiLayer.findViewById(R.id.extend_and_background_layout);
        mUiPausePlay = (PlayIconView) mUiLayer.findViewById(R.id.pause_play);
        mUiSeekbarLayout = (RelativeLayout) mUiLayer.findViewById(R.id.seekbar_layout);

        mUiPausePlay.setIconState(PlayIconDrawable.IconState.PAUSE);
        mUiPausePlay.setVisibility(View.INVISIBLE);

        mUiExtendBackground = (ImageView) mUiLayer.findViewById(R.id.extend_background);
        mUiExtendButton = (ImageView) mUiLayer.findViewById(R.id.extend_button);
        mUiSeekBar = (SeekBar) mUiLayer.findViewById(R.id.seekbar);
        mUiSeekbarEndTimeLabel = (TextView) mUiLayer.findViewById(R.id.seekbar_end_time_label);

        mDebugUiPlayerId = (TextView) mDebugUiLayer.findViewById(R.id.player_id);
        mDebugUiPartName = (TextView) mDebugUiLayer.findViewById(R.id.part_name);
        mDebugUiStartTime = (TextView) mDebugUiLayer.findViewById(R.id.start_time);
        mDebugUiEndTime = (TextView) mDebugUiLayer.findViewById(R.id.end_time);

        mUiPrevoiusOptionVisualIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentStateContainer.isPreviousAvailable() == false){
                    return;
                }
                if (mDeckOfThreeViewPagerInterface == null) {
                    mElasticPlayerView.dispatchEvent(StateEvent.PREVIOUS, null);
                }else{
                    mDeckOfThreeViewPagerInterface.setViewPagePosition(ViewPagePosition.PREVIOUS, true);
                }
            }
        });
        mUiNextOptionVisualIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentStateContainer.isNextAvailable() == false){
                    return;
                }
                if (mDeckOfThreeViewPagerInterface == null) {
                    mElasticPlayerView.dispatchEvent(StateEvent.NEXT, null);
                }else{
                    mDeckOfThreeViewPagerInterface.setViewPagePosition(ViewPagePosition.NEXT, true);
                }
            }
        });

        mUiExtendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimeLastTapMs = mPlaybackCurrentTimeMs;
                mElasticPlayerView.dispatchEvent(StateEvent.EXTEND, null);
                toggleVisibilityOfAllViews(SHORT_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, FORCE_SHOW, IS_SHOW_UI_EVERY_NEXT_PREVIOUS);
            }
        });
        mUiPausePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimeLastTapMs = mPlaybackCurrentTimeMs;
                mElasticPlayerView.dispatchEvent(StateEvent.TOGGLE_PAUSE_PLAY, null);
                toggleVisibilityOfAllViews(SHORT_FADE_MS, INCLUDE_PAUSE_PLAY, FORCE_SHOW, IS_SHOW_UI_EVERY_NEXT_PREVIOUS);
            }
        });

        if (mDeckOfThreeViewPagerInterface == null) {
            mGestureDetector = new GestureDetectorCompat(mElasticPlayerView.getContext(), new GestureListener());

            mUiLayer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mGestureDetector != null) {
                        mGestureDetector.onTouchEvent(event);
                        return true;
                    }
                    return false;
                }
            });
        }else{
            mElasticPlayerView.getUiLayer().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mElasticPlayerView.dispatchEvent(StateEvent.SINGLE_TAP, null);
                }
            });
            mElasticPlayerView.getUiLayer().setSoundEffectsEnabled(false);
        }


        mUiSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean isFromUser) {
                if (isFromUser == false){
                    return;
                }
                mIsUserSeekingPending = true;
                mTimeLastTapMs = mPlaybackCurrentTimeMs;
                mUserRequestedSeekPosition = progress - (mCurrentStateContainer.isExtendUiMode()?0: mCurrentStateContainer.getPlaybackCurrentTimeOffsetMs());
                updateUiPlaybackTimesIfNeeded(mCurrentStateContainer.isExtendUiMode(), mUserRequestedSeekPosition, mCurrentStateContainer.getPlaybackCurrentTimeOffsetMs(), mPlaybackEndTimeMs , mCurrentStateContainer.getPlaybackEndTimeOffsetMs());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsUserHoldingSeek = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mElasticPlayerView.dispatchEvent(StateEvent.SEEK, (Long)mUserRequestedSeekPosition);
                mTimeLastTapMs = mUserRequestedSeekPosition;
                seekBar.postDelayed(RUNNABLE_RESET_USER_REQUESTED_SEEK_POSITION_MAX, RESET_USER_REQUESTED_SEEK_POSTITON_MAX_MS);
                toggleVisibilityOfAllViews(SHORT_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, FORCE_SHOW, IS_SHOW_UI_EVERY_NEXT_PREVIOUS);
                mIsUserHoldingSeek = false;
            }
        });
        processUiEvent(UiEvent.SET_EXTEND_AVAILABILITY, stateContainer);
    }

    private void toggleVisibilityOfAllViews(final long fadeAnimationDuration, final boolean includePlayPause, Boolean forceShow, Boolean isShowUiEveryItemSwitch) {
        float startAlpha;
        float endAlpha;
        final boolean endResultIsOnScreen = ((forceShow==null || forceShow==false) ? !mIsViewVisible : forceShow);
        mIsViewVisible = endResultIsOnScreen;
        if (endResultIsOnScreen) {
            startAlpha = 0F;
            endAlpha = 1F;
        }else{
            startAlpha = 1F;
            endAlpha = 0F;
        }

        if (isShowUiEveryItemSwitch) {
            fadeHelper(mUiSeekbarLayout, fadeAnimationDuration, startAlpha, endAlpha, endResultIsOnScreen);
        }

        if (isShowUiEveryItemSwitch) {
            if (mCurrentStateContainer.isPreviousAvailable()) {
                fadeHelper(mUiPrevoiusOptionVisualIndicator, fadeAnimationDuration, startAlpha, endAlpha, endResultIsOnScreen);
            }else{
                mUiPrevoiusOptionVisualIndicator.setVisibility(HIDDEN_VIEW);
            }
            if (mCurrentStateContainer.isNextAvailable()) {
                fadeHelper(mUiNextOptionVisualIndicator, fadeAnimationDuration, startAlpha, endAlpha, endResultIsOnScreen);
            }else{
                mUiNextOptionVisualIndicator.setVisibility(HIDDEN_VIEW);
            }
        }
        else {
            if (mCurrentStateContainer.getCurrentItemNumber() == 2 ) {
                viewStateCopier(mUiNextOptionVisualIndicator, mUiPrevoiusOptionVisualIndicator);
            }
            else if (mCurrentStateContainer.getCurrentItemNumber() == mCurrentStateContainer.getTotalItemCount()-1) {
                viewStateCopier(mUiPrevoiusOptionVisualIndicator, mUiNextOptionVisualIndicator);
            }
            else if (mCurrentStateContainer.getCurrentItemNumber() == 1 ) {
                mUiPrevoiusOptionVisualIndicator.setVisibility(View.INVISIBLE);
            }
            else if (mCurrentStateContainer.getCurrentItemNumber() == mCurrentStateContainer.getTotalItemCount()) {
                mUiNextOptionVisualIndicator.setVisibility(View.INVISIBLE);
            }
        }

        if (isShowUiEveryItemSwitch) {
            if (includePlayPause || (endResultIsOnScreen == false && isViewVisibleHelper(mUiPausePlay))) {
                startAlpha = startAlpha * PLAY_PAUSE_MAX_ALPHA;
                endAlpha = endAlpha * PLAY_PAUSE_MAX_ALPHA;
                fadeHelper(mUiPausePlay, fadeAnimationDuration, startAlpha, endAlpha, endResultIsOnScreen);
            }
        }
    }

    private boolean isViewVisibleHelper(View view){
        float maxAlpha = 1F;
        if (view == mUiPausePlay){
            maxAlpha = PLAY_PAUSE_MAX_ALPHA;
        }
        if (view.getAlpha() >= maxAlpha && view.getVisibility() == View.VISIBLE){
            return true;
        }else{
            return false;
        }
    }

    private void fadeHelper(final View view, long fadeDuration, final float startAlpha, final float endAlpha, final boolean endResultIsOnScreen){
        if (endResultIsOnScreen && isViewVisibleHelper(view)){
            return;
        }else if(endResultIsOnScreen == false && isViewVisibleHelper(view) == false){
            return;
        }
        view.clearAnimation();
        view.animate().withLayer()
                .setDuration(fadeDuration)
                .alpha(endAlpha)
                .withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        view.setAlpha(startAlpha);
                        view.setVisibility(View.VISIBLE);
                    }
                })
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(endResultIsOnScreen?View.VISIBLE: HIDDEN_VIEW);
                        view.setAlpha(endAlpha);
                    }
                });
    }

    private void viewStateCopier(View source, View target) {
        target.setVisibility(source.getVisibility());
        float currentAlpha = source.getAlpha();
        target.setAlpha(currentAlpha);
        if (currentAlpha < 1f) {
            target.animate().withLayer().alpha(0f).setDuration((long)(LONG_FADE_MS*currentAlpha));
        }
    }

    @Override
    public void releaseAll() {

    }

    @Override
    public void processUiEvent(UiEvent event, StateContainer stateContainer) {
        mCurrentStateContainer = stateContainer;
        //We must turn it into a 0 base index
        int playingHeader0Base = stateContainer.getCurrentItemNumber() - 1;
        switch (event) {
            case INIT_UI : {
                initialViewMode(stateContainer);
                if (mDeckOfThreeViewPagerInterface != null){
                    mDeckOfThreeViewPagerInterface.setPreviewPictureUrls(stateContainer.getAllItemPreviewImageUrls(), playingHeader0Base);
                }
                updateSegmentIndicator(playingHeader0Base, stateContainer.getTotalItemCount());
                resetUiPlaybackTimesCache();
                mSwipeListening = true;
                toggleVisibilityOfAllViews(SHORT_FADE_MS, INCLUDE_PAUSE_PLAY, FORCE_SHOW, true);
                break;
            }
            case NEXT : {
                toggleVisibilityOfAllViews(SHORT_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, FORCE_SHOW, IS_SHOW_UI_EVERY_NEXT_PREVIOUS);
                resetUiPlaybackTimesCache();
                break;
            }
            case PREVIOUS : {
                toggleVisibilityOfAllViews(SHORT_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, FORCE_SHOW, IS_SHOW_UI_EVERY_NEXT_PREVIOUS);
                resetUiPlaybackTimesCache();
                break;
            }
            case INTRO_MODE : {
                mUiExtendButtonLayout.setVisibility(View.INVISIBLE);
                updateSegmentIndicator(playingHeader0Base, stateContainer.getTotalItemCount());
                startBoxAnimation(-1);
                break;
            }
            case EXTEND_MODE : {
                mUiExtendButtonLayout.setVisibility(View.INVISIBLE);
                mTimeLastTapMs = mPlaybackCurrentTimeMs;
                startBoxAnimation(stateContainer.getCurrentItemNumber()-1);
                toggleVisibilityOfAllViews(SHORT_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, FORCE_SHOW, true);
                break;
            }
            case PLAYBACK_TIMES_UPDATE: {
                if (mIsUserHoldingSeek == false) {
                    boolean didUpdateTime = false;
                    if (mIsUserSeekingPending == false) {
                        didUpdateTime = updateUiPlaybackTimesIfNeeded(stateContainer.isExtendUiMode(),
                                stateContainer.getPlaybackCurrentTimeMs(), stateContainer.getPlaybackCurrentTimeOffsetMs(),
                                stateContainer.getPlaybackEndTimeMs(), stateContainer.getPlaybackEndTimeOffsetMs());

                    } else if (mUserRequestedSeekPosition != DEFAULT_USER_REQUESTED_SEEK_POSITION) {
                        long diffReq = Math.abs(stateContainer.getPlaybackCurrentTimeMs() - mUserRequestedSeekPosition);
                        if (diffReq <= USER_REQUESTED_SEEK_POSTITON_WIGGLE_MS) {
                            mIsUserSeekingPending = false;
                            mUserRequestedSeekPosition = DEFAULT_USER_REQUESTED_SEEK_POSITION;
                            didUpdateTime = updateUiPlaybackTimesIfNeeded(stateContainer.isExtendUiMode(),
                                    stateContainer.getPlaybackCurrentTimeMs(), stateContainer.getPlaybackCurrentTimeOffsetMs(),
                                    stateContainer.getPlaybackEndTimeMs(), stateContainer.getPlaybackEndTimeOffsetMs());
                        }

                    }

                    if (didUpdateTime){
                        if (mIsViewVisible && mPlaybackCurrentTimeMs - mTimeLastTapMs > AUTO_FADE_DELAY_MS){
                            toggleVisibilityOfAllViews(LONG_FADE_MS, DO_NOT_INCLUDE_PAUSE_PLAY, DO_NOT_FORCE_SHOW, true);
                        }
                    }
                }
                break;
            }
            case DEBUG_UPDATE : {
                mDebugUiPlayerId.setText("pId:" + String.valueOf(stateContainer.getDebugPlayerID()));
                mDebugUiPartName.setText(String.valueOf(stateContainer.getActiveBasicSegment().getName()));
                mDebugUiStartTime.setText(Utils.millisecondsToString(stateContainer.getActiveBasicSegment().getIn()));
                mDebugUiEndTime.setText(Utils.millisecondsToString(stateContainer.getActiveBasicSegment().getOut()));
                break;
            }
            case SWITCH_TO_PLAY_BUTTON: {
                if (mUiPausePlay.getIconState() != PlayIconDrawable.IconState.PLAY) {
                    mUiPausePlay.animateToState(PlayIconDrawable.IconState.PLAY);
                }
                break;
            }
            case SWITCH_TO_PAUSE_BUTTON: {
                if (mUiPausePlay.getIconState() != PlayIconDrawable.IconState.PAUSE) {
                    mUiPausePlay.animateToState(PlayIconDrawable.IconState.PAUSE);
                }
                break;
            }
            case SINGLE_TAP: {
                mTimeLastTapMs = mPlaybackCurrentTimeMs;
                toggleVisibilityOfAllViews(SHORT_FADE_MS, INCLUDE_PAUSE_PLAY, DO_NOT_FORCE_SHOW, true);
                break;
            }
            case SET_EXTEND_AVAILABILITY: {
                mUiExtendButtonLayout.setVisibility(stateContainer.isExtendAvailable() ? View.VISIBLE : View.INVISIBLE);

                break;
            }
        }
    }

    /**
     * use
     * @param playingHeader to reset indicators to initial size use -1
     */
    public void startBoxAnimation(int playingHeader){
        if (playingHeader < 0){
            ExtendCurrentIndicatorAnimator.resetViews(mSegmentIndicatorsSimpleArray);
            return;
        }
        LayoutTransition layoutTransition = new LayoutTransition();
        ExtendCurrentIndicatorAnimator mAnimation = new ExtendCurrentIndicatorAnimator(mSegmentIndicatorsSimpleArray, playingHeader);
        mAnimation.setDuration(1000);
        layoutTransition.setAnimator(LayoutTransition.CHANGING, mAnimation);
        mUiSegmentIndicatorsLayout.setLayoutTransition(layoutTransition);
        mAnimation.start();
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (mSwipeListening == false) {
                return false;
            }
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                mElasticPlayerView.dispatchEvent(StateEvent.NEXT, null);
                return true; // Right to left

            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                mElasticPlayerView.dispatchEvent(StateEvent.PREVIOUS, null);
                return true; // Left to right
            }
            return false;
        }
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            mElasticPlayerView.dispatchEvent(StateEvent.SINGLE_TAP, null);
            return super.onSingleTapConfirmed(e);
        }
    }

    private void initSegmentIndicator(final int headersNumber) {
        mSegmentIndicators = new ArrayList<View>(headersNumber);
        Context context = mElasticPlayerView.getContext();
        final int viewWidth = (mUiLayer.getWidth())/headersNumber ;

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(viewWidth, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        LinearLayout.LayoutParams lpForSeparator = new LinearLayout.LayoutParams(Math.round(mPixelsInOneDPfloat), LinearLayout.LayoutParams.MATCH_PARENT);

        mSegmentIndicatorsSimpleArray = new View[headersNumber];
        View indicatorSeparatorFirst = inflate(context, R.layout.skin1_segment_indicator_separator_view, null);
        View indicatorSeparatorLast = inflate(context, R.layout.skin1_segment_indicator_separator_view, null);


        for (int i = 0 ; i < headersNumber ; i++) {
            if (i == 0 ) {
                mUiSegmentIndicatorsLayout.addView(indicatorSeparatorFirst, lpForSeparator);
            }
            View v2 = inflate(context, R.layout.skin1_segment_indicator, null);
            mSegmentIndicators.add(v2);
            mSegmentIndicatorsSimpleArray[i] = v2;
            mUiSegmentIndicatorsLayout.addView(v2);
            v2.setLayoutParams(lp);
            if (i == headersNumber-1 ) {
                mUiSegmentIndicatorsLayout.addView(indicatorSeparatorLast, lpForSeparator);
            }
        }
    }

    /**
     *
     * @param playingHeader Must be given as 0 base index
     * @param totalHeaders
     */
    private void updateSegmentIndicator(int playingHeader, int totalHeaders){
        if (mSegmentIndicators == null){
            initSegmentIndicator(totalHeaders);
        }

        for(int i = 0 ; i < mSegmentIndicators.size() ; i++) {
            View v = mSegmentIndicators.get(i);
            if (i == playingHeader) {
                v.setBackgroundResource(R.drawable.skin1_segment_indicator_active);
            }else {
                v.setBackgroundResource(R.drawable.skin1_segment_indicator_inactive);
            }
        }
    }

    private void resetUiPlaybackTimesCache(){
        mTimeLastTapMs = -1;
        mPlaybackCurrentTimeMs = -1;
        mPlaybackEndTimeMs = -1;
        mIsUserSeekingPending = false;
        mUserRequestedSeekPosition = DEFAULT_USER_REQUESTED_SEEK_POSITION;
    }

    private boolean updateUiPlaybackTimesIfNeeded(boolean isExtendedMode, long playbackCurrentTimeMs, long playbackCurrentTimeOffsetMs, long playbackEndTimeMs, long playbackEndTimeOffsetMs) {
        boolean didUpdate = false;
        if (mPlaybackCurrentTimeMs != playbackCurrentTimeMs){
            didUpdate = true;
            mPlaybackCurrentTimeMs = playbackCurrentTimeMs;
            long remainingPlaybackTime;
            if (isExtendedMode) {
                mUiSeekBar.setProgress((int)playbackCurrentTimeMs);
                remainingPlaybackTime = playbackEndTimeMs-playbackCurrentTimeMs;
                if (remainingPlaybackTime >=0) {
                    mUiSeekbarEndTimeLabel.setText(Utils.millisecondsToString(remainingPlaybackTime));
                }
            }else {
                mUiSeekBar.setProgress((int) (playbackCurrentTimeMs + playbackCurrentTimeOffsetMs));
                remainingPlaybackTime = (playbackEndTimeMs+playbackEndTimeOffsetMs+playbackCurrentTimeOffsetMs) - (playbackCurrentTimeMs+ playbackCurrentTimeOffsetMs);
                if (remainingPlaybackTime >=0) {
                    mUiSeekbarEndTimeLabel.setText(Utils.millisecondsToString(remainingPlaybackTime));
                }
            }
        }

        if (mPlaybackEndTimeMs != playbackEndTimeMs){
            mPlaybackEndTimeMs = playbackEndTimeMs;
            if (isExtendedMode) {
                mUiSeekBar.setMax((int)playbackEndTimeMs);
            }else{
                mUiSeekBar.setMax((int)(playbackEndTimeMs+playbackEndTimeOffsetMs+playbackCurrentTimeOffsetMs));
            }
        }

        return didUpdate;
    }

    public static class ExtendCurrentIndicatorAnimator extends ValueAnimator {

        public static void resetViews(final View[] views){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            for (int i = 0; i < views.length; i++) {
                views[i].setLayoutParams(layoutParams);
            }
        }

        public ExtendCurrentIndicatorAnimator(final View[] views, final int index) {
            super.setFloatValues(1f, 2f, 1f, 3f, 2f, 4f);

            resetViews(views);

            this.addUpdateListener(new AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float weight = (float) valueAnimator.getAnimatedValue();
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, weight);
                    views[index].setLayoutParams(layoutParams);
                }
            });
        }
    }
}