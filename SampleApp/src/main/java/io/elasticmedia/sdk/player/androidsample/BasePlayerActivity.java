package io.elasticmedia.sdk.player.androidsample;

import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import io.elasticmedia.sdk.player.android.library.ElasticPlayerView;
import io.elasticmedia.sdk.player.android.library.utils.Utils;


public class BasePlayerActivity extends AppCompatActivity {

    private final static String TAG = BasePlayerActivity.class.getSimpleName();

    public static final String BUNDLE_CONTENT_URL = "BUNDLE_CONTENT_URL";
    public static final String BUNDLE_IS_DEBUG_VERBOSE = "BUNDLE_IS_DEBUG_VERBOSE";
    public static final String BUNDLE_IS_SERVER_DEV = "BUNDLE_IS_SERVER_DEV";
    public static final String BUNDLE_IS_TFX_FORCED_OFF = "BUNDLE_IS_TFX_FORCED_OFF";
    public static final String BUNDLE_EPISODE_ID = "BUNDLE_EPISODE_ID";
    public static final String BUNDLE_EXTERNAL_USER_ID = "BUNDLE_EXTERNAL_USER_ID";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        Bundle arguments = getIntent().getExtras();
        String url = arguments.getString(BUNDLE_CONTENT_URL);
        boolean isDebugVerbose = arguments.getBoolean(BUNDLE_IS_DEBUG_VERBOSE);

        if (isDebugVerbose) {
            if (Utils.isInAnyKindTestMode(this) == false) {
                Toast.makeText(this, url, Toast.LENGTH_LONG).show();
            }
        }

        ElasticPlayerView.setIsProductionBuild(!isDebugVerbose);

    }

}
