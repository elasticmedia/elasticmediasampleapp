package io.elasticmedia.sdk.player.androidsample;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import io.elasticmedia.sdk.player.android.library.ElasticPlayerView;
import io.elasticmedia.sdk.player.android.library.ElasticSdkExtListener;
import io.elasticmedia.sdk.player.android.library.transitionEffects.TransitionEffectProperties;
import io.elasticmedia.sdk.player.android.library.utils.Utils;


public class PlayerActivity extends BasePlayerActivity implements ElasticSdkExtListener {

    private final static String TAG = PlayerActivity.class.getSimpleName();
    final String LOG_PREPEND = "EM-SDK~";


    public static final String BUNDLE_JSON_SAVED_STATE = "BUNDLE_JSON_SAVED_STATE";

    private final Handler mUiHandler = new Handler(Looper.getMainLooper());
    private FrameLayout mUiActivityParent;
    private ElasticPlayerView mUiElasticPlayerView;
    private String mSavedStateStringified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_player);
        mUiActivityParent = (FrameLayout)findViewById(R.id.player_activity_parent);

        Bundle arguments = getIntent().getExtras();
        String mDataId = arguments.getString(BUNDLE_EPISODE_ID);
        String url = arguments.getString(BUNDLE_CONTENT_URL);
        String externalUserID = arguments.getString(BUNDLE_EXTERNAL_USER_ID);
        boolean isDebugVerbose = arguments.getBoolean(BUNDLE_IS_DEBUG_VERBOSE);
        boolean isServerDev = arguments.getBoolean(BUNDLE_IS_SERVER_DEV);
        boolean isTFxForcedOff = arguments.getBoolean(BUNDLE_IS_TFX_FORCED_OFF);

        mSavedStateStringified = null;

        if (savedInstanceState != null) {
            mSavedStateStringified = savedInstanceState.getString(BUNDLE_JSON_SAVED_STATE);
            if (TextUtils.isEmpty(mSavedStateStringified) == false){
                resumeFromState(mSavedStateStringified);
                mSavedStateStringified = null;
                return;
            }
        }

        mUiElasticPlayerView = new ElasticPlayerView(this);
        mUiElasticPlayerView
                .setIsDebugVerbose(isDebugVerbose)
                .setIsServerDev(isServerDev)
                .setPublisherId(BuildConfig.PUBLISHER_ID_CH2)
                .setExternalUserId(externalUserID);


        mUiElasticPlayerView
                .setTransitionEffectNext(TransitionEffectProperties.LAST_FRAME_SLIDE)
                .setTransitionEffectPrevious(TransitionEffectProperties.LAST_FRAME_SLIDE)
                .setTransitionEffectNoAction(null)
                .setTransitionEffectFileNextNoAction(null);

        addElasticPlayerView(mUiElasticPlayerView);
        mUiElasticPlayerView.play(mDataId, url, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mUiElasticPlayerView != null) {
            mSavedStateStringified = mUiElasticPlayerView.getState();
            if (mUiElasticPlayerView.isReleased() == false) {
                mUiElasticPlayerView.releaseAll();
            }
            removeElasticPlayerView(mUiElasticPlayerView);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }


    @Override
    protected void onResume() {
        super.onResume();
        resumeFromState(mSavedStateStringified);
    }

    private void resumeFromState(String savedState){
        if (savedState == null){
            return;
        }
        mUiElasticPlayerView = new ElasticPlayerView(this);
        addElasticPlayerView(mUiElasticPlayerView);
        mUiElasticPlayerView.resumeFromState(savedState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mUiElasticPlayerView != null) {
            mSavedStateStringified = mUiElasticPlayerView.getState();
            outState.putString(BUNDLE_JSON_SAVED_STATE, mSavedStateStringified);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    private void removeElasticPlayerView(ElasticPlayerView elasticPlayerView){
        if (elasticPlayerView != null && elasticPlayerView.getParent() instanceof ViewGroup) {
            ((ViewGroup)(elasticPlayerView.getParent())).removeView(elasticPlayerView);
        }
    }

    /**
     * Implicitly creates the ViewPager if needed
     * @param elasticPlayerView
     */
    private void addElasticPlayerView(final ElasticPlayerView elasticPlayerView){
        if (elasticPlayerView.getParent() != null){
            Log.w(LOG_PREPEND + TAG, "removeElasticPlayerView() already has a parent");

            return;
        }

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);

        mUiActivityParent.addView(elasticPlayerView, layoutParams);

        mUiElasticPlayerView.setEventListener(this);

        mUiElasticPlayerView.setupPepsinUi();
    }

    @Override
    public void onEpisodeEnded(final String errorString) {
        Log.d(LOG_PREPEND + TAG, "onEpisodeEnded(): " + errorString);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_PREPEND + TAG, "onEpisodeEnded() runnable: " + errorString);
                if (TextUtils.isEmpty(errorString) == false && Utils.isInAnyKindTestMode(PlayerActivity.this.getApplicationContext()) == false){
                    Toast.makeText(PlayerActivity.this, errorString, Toast.LENGTH_LONG).show();
                }
                if (mUiElasticPlayerView != null && mUiElasticPlayerView.isReleased() == false) {
                    mUiElasticPlayerView.releaseAll();
                }
                if (PlayerActivity.this.isFinishing() == false) {
                    PlayerActivity.this.finish();
                }
            }
        });
    }

    @Override
    public void onNextItem(final int itemNumber) {

    }

    @Override
    public void onPreviousItem(final int itemNumber) {

    }

    /**
     * @return TRUE if should AutoPlay to Next Item
     */
    @Override
    public boolean onItemEnded() {
        return true;
    }
}
